<?php

namespace App\Entity;

use App\Repository\JeuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JeuRepository::class)]
class Jeu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateCreated = null;

    #[ORM\Column(length: 255)]
    private ?string $image = null;

    #[ORM\ManyToOne(inversedBy: 'jeux')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Editeur $linkWithEditeur = null;

    #[ORM\ManyToMany(targetEntity: Categorie::class, inversedBy: 'jeux')]
    private Collection $linkToCategorie;

    #[ORM\ManyToOne(inversedBy: 'jeux')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Classification $linkToClassification = null;

    #[ORM\ManyToOne(inversedBy: 'jeux')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Plateforme $linkToPlateforme = null;

    #[ORM\OneToMany(mappedBy: 'jeu', targetEntity: Avis::class, orphanRemoval: true)]
    private Collection $avisSurLeJeu;

    public function __construct()
    {
        $this->linkToCategorie = new ArrayCollection();
        $this->avisSurLeJeu = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getLinkWithEditeur(): ?Editeur
    {
        return $this->linkWithEditeur;
    }

    public function setLinkWithEditeur(?Editeur $linkWithEditeur): self
    {
        $this->linkWithEditeur = $linkWithEditeur;

        return $this;
    }

    /**
     * @return Collection<int, Categorie>
     */
    public function getLinkToCategorie(): Collection
    {
        return $this->linkToCategorie;
    }

    public function addLinkToCategorie(Categorie $linkToCategorie): self
    {
        if (!$this->linkToCategorie->contains($linkToCategorie)) {
            $this->linkToCategorie->add($linkToCategorie);
        }

        return $this;
    }

    public function removeLinkToCategorie(Categorie $linkToCategorie): self
    {
        $this->linkToCategorie->removeElement($linkToCategorie);

        return $this;
    }

    public function getLinkToClassification(): ?Classification
    {
        return $this->linkToClassification;
    }

    public function setLinkToClassification(?Classification $linkToClassification): self
    {
        $this->linkToClassification = $linkToClassification;

        return $this;
    }

    public function getLinkToPlateforme(): ?Plateforme
    {
        return $this->linkToPlateforme;
    }

    public function setLinkToPlateforme(?Plateforme $linkToPlateforme): self
    {
        $this->linkToPlateforme = $linkToPlateforme;

        return $this;
    }

    /**
     * @return Collection<int, Avis>
     */
    public function getAvisSurLeJeu(): Collection
    {
        return $this->avisSurLeJeu;
    }

    public function addAvisSurLeJeu(Avis $avisSurLeJeu): self
    {
        if (!$this->avisSurLeJeu->contains($avisSurLeJeu)) {
            $this->avisSurLeJeu->add($avisSurLeJeu);
            $avisSurLeJeu->setJeu($this);
        }

        return $this;
    }

    public function removeAvisSurLeJeu(Avis $avisSurLeJeu): self
    {
        if ($this->avisSurLeJeu->removeElement($avisSurLeJeu)) {
            // set the owning side to null (unless already changed)
            if ($avisSurLeJeu->getJeu() === $this) {
                $avisSurLeJeu->setJeu(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}

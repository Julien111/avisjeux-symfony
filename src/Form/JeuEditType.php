<?php

namespace App\Form;

use App\Entity\Jeu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JeuEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('dateCreated', DateType::class, [    
                'format' => 'dd-MM-yyyy',            
                'years' => range(date('Y'), date('Y') - 100),
            ])            
            ->add('linkWithEditeur')
            ->add('linkToCategorie')
            ->add('linkToClassification')
            ->add('linkToPlateforme')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Jeu::class,
        ]);
    }
}

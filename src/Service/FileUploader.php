<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader implements UploaderInterface{

  private SluggerInterface $slugger;

  private string $uploadsAbsoluteDir;

  private string $uploadsRelativeDir;

  public function __construct(SluggerInterface $slugger, string $uploadsAbsoluteDir, string $uploadsRelativeDir){
      $this->slugger = $slugger;
      $this->uploadsAbsoluteDir = $uploadsAbsoluteDir;
      $this->uploadsRelativeDir = $uploadsRelativeDir;

  }

  public function upload(UploadedFile $file): string
  {
     $filename = sprintf(
            "%s_%s.%s", 
            $this->slugger->slug($file->getClientOriginalName()),
            uniqid(),
            $file->getClientOriginalExtension()         
            );
    //une fois la modification du nom pour qu'il soit unique   
    $file->move($this->uploadsAbsoluteDir, $filename);
    //on retourne le chemin du fichier
    return $this->uploadsRelativeDir . "/" . $filename;
  }


}
<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230526133135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE jeu_categorie (jeu_id INT NOT NULL, categorie_id INT NOT NULL, PRIMARY KEY(jeu_id, categorie_id))');
        $this->addSql('CREATE INDEX IDX_E337000C8C9E392E ON jeu_categorie (jeu_id)');
        $this->addSql('CREATE INDEX IDX_E337000CBCF5E72D ON jeu_categorie (categorie_id)');
        $this->addSql('ALTER TABLE jeu_categorie ADD CONSTRAINT FK_E337000C8C9E392E FOREIGN KEY (jeu_id) REFERENCES jeu (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jeu_categorie ADD CONSTRAINT FK_E337000CBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE avis ADD jeu_id INT NOT NULL');
        $this->addSql('ALTER TABLE avis ADD CONSTRAINT FK_8F91ABF08C9E392E FOREIGN KEY (jeu_id) REFERENCES jeu (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8F91ABF08C9E392E ON avis (jeu_id)');
        $this->addSql('ALTER TABLE jeu ADD link_with_editeur_id INT NOT NULL');
        $this->addSql('ALTER TABLE jeu ADD link_to_classification_id INT NOT NULL');
        $this->addSql('ALTER TABLE jeu ADD link_to_plateforme_id INT NOT NULL');
        $this->addSql('ALTER TABLE jeu ADD CONSTRAINT FK_82E48DB5E86E3287 FOREIGN KEY (link_with_editeur_id) REFERENCES editeur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jeu ADD CONSTRAINT FK_82E48DB54DFA0192 FOREIGN KEY (link_to_classification_id) REFERENCES classification (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jeu ADD CONSTRAINT FK_82E48DB5F1F2272C FOREIGN KEY (link_to_plateforme_id) REFERENCES plateforme (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_82E48DB5E86E3287 ON jeu (link_with_editeur_id)');
        $this->addSql('CREATE INDEX IDX_82E48DB54DFA0192 ON jeu (link_to_classification_id)');
        $this->addSql('CREATE INDEX IDX_82E48DB5F1F2272C ON jeu (link_to_plateforme_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE jeu_categorie DROP CONSTRAINT FK_E337000C8C9E392E');
        $this->addSql('ALTER TABLE jeu_categorie DROP CONSTRAINT FK_E337000CBCF5E72D');
        $this->addSql('DROP TABLE jeu_categorie');
        $this->addSql('ALTER TABLE avis DROP CONSTRAINT FK_8F91ABF08C9E392E');
        $this->addSql('DROP INDEX IDX_8F91ABF08C9E392E');
        $this->addSql('ALTER TABLE avis DROP jeu_id');
        $this->addSql('ALTER TABLE jeu DROP CONSTRAINT FK_82E48DB5E86E3287');
        $this->addSql('ALTER TABLE jeu DROP CONSTRAINT FK_82E48DB54DFA0192');
        $this->addSql('ALTER TABLE jeu DROP CONSTRAINT FK_82E48DB5F1F2272C');
        $this->addSql('DROP INDEX IDX_82E48DB5E86E3287');
        $this->addSql('DROP INDEX IDX_82E48DB54DFA0192');
        $this->addSql('DROP INDEX IDX_82E48DB5F1F2272C');
        $this->addSql('ALTER TABLE jeu DROP link_with_editeur_id');
        $this->addSql('ALTER TABLE jeu DROP link_to_classification_id');
        $this->addSql('ALTER TABLE jeu DROP link_to_plateforme_id');
    }
}
